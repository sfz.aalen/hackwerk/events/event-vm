# /etc/nixos/flake.nix
{
  description = "event-vm";

  inputs = {
    nixpkgs = {
      url = "github:NixOS/nixpkgs/nixos-unstable";
    };
  };

  outputs = { self, nixpkgs, ... }: {
    nixosConfigurations = {
      eventvm = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = [
          ./hardware-configuration.nix
          ./modules
        ];
      };
    };
  };
}
