{ pkgs, user, uid }: pkgs.writeShellApplication {
  name = "install";
  text = ''
    set -eo pipefail
    
    DISK="/dev/sda"

    # create new table
    parted --script "$DISK" -- mklabel gpt

    # 0% automatically offsets for optimal performance
    parted --script "$DISK" -- mkpart ESP fat32 0% 512MB
    parted --script "$DISK" -- set 1 esp on

    # main partition
    parted --script "$DISK" -- mkpart primary 10GB 100%

    mkfs.fat -F 32 "$DISK"1
    fatlabel "$DISK"1 NIXBOOT
    mkfs.ext4 "$DISK"2 -L NIXROOT
    mount /dev/disk/by-label/NIXROOT /mnt
    mkdir -p /mnt/boot
    mount /dev/disk/by-label/NIXBOOT /mnt/boot

    nixos-install --flake "gitlab:sfz.aalen%2Fhackwerk%2Fevents/event-vm#eventvm" --no-root-password
  '';
}
