{
    services.prometheus = {
        enable = true;
        port = 9001;
        scrapeConfigs = [
        {
            job_name = "node_pbx";
            scrape_interval = "10s";
            static_configs = [
            {
              targets = [
                "10.44.42.10:8095"
              ];
              labels = {
                alias = "asterisk";
              };
            }
          ];
        }
        {
            job_name = "node_fw-01";
            scrape_interval = "10s";
            static_configs = [
            {
              targets = [
                "10.44.42.2:9100"
              ];
              labels = {
                alias = "firewall";
              };
            }
          ];
        }
        ];
    };
    services.grafana = {
        enable = true;
        settings = {
            default.ini = ''
                # default section
                instance_name = Eventgrafana

                [security]
                admin_user = admin

                [plugin.grafana-image-renderer]
                rendering_ignore_https_errors = true

                [feature_toggles]
                enable = newNavigation
            '';
            server = {
                domain = "grafana.event";
                port = 3000;
                http_addr = "0.0.0.0";
            };
        };
        
    };

}