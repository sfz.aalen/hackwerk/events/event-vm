{
    services.kea.dhcp4 = {
        enable = true;
        settings = {
            option-def = [
                {
                  name = "Gatekeeper-Identifier";
                  code = 201;
                  space = "vendor-encapsulated-options-space";
                  type = "string";
                  array = false;
                }
				        {
                  name = "VOIP-Protocol";
                  code = 220;
                  space = "vendor-encapsulated-options-space";
                  type = "string";
                  array = false;
                }
                {
                  name = "Coder";
                  code = 203;
                  space = "vendor-encapsulated-options-space";
                  type = "string";
                  array = false;
                }
                {
                  name = "Dial-Tones";
                  code = 210;
                  space = "vendor-encapsulated-options-space";
                  type = "uint8";
                  array = false;
                }
                {
                  name = "Language";
                  code = 204;
                  space = "vendor-encapsulated-options-space";
                  type = "string";
                  array = false;
                }
                {
                  name = "AM-PM-Clock";
                  code = 213;
                  space = "vendor-encapsulated-options-space";
                  type = "uint8";
                  array = false;
                }
                {
                  name = "dect-omm-ip";
                  code = 19;
                  space = "vendor-encapsulated-options-space";
                  type = "ipv4-address";
                  array = false;
                }
            ];
            client-classes = [
                {
                    name = "innovaphone";
                    test = "option[vendor-class-identifier].text == '1.3.6.1.4.1.6666'";
                    option-data = [
                        {
                            name = "vendor-encapsulated-options";
                            always-send = true;
                        }
                        {
                            name = "Gatekeeper-Identifier";
                            space = "vendor-encapsulated-options-space";
                            data = "10.40.42.10";
                        }
                        {
                            name = "VOIP-Protocol";
                            space = "vendor-encapsulated-options-space";
                            data = "SIP/UDP";
                        }
                        {
                            name = "Coder";
                            space = "vendor-encapsulated-options-space";
                            data = "G711A,20,/G722,20,";
                        }
                        {
                            name = "Dial-Tones";
                            space = "vendor-encapsulated-options-space";
                            data = "1";
                        }
                        {
                            name = "Language";
                            space = "vendor-encapsulated-options-space";
                            data = "ger";
                        }
                        {
                            name = "AM-PM-Clock";
                            space = "vendor-encapsulated-options-space";
                            data = "0";
                        }
                    ];
                }
                {
                    name = "dect";
                    test = "option[vendor-class-identifier].text == 'OpenMobility3G'";
                    option-data = [
                        {
                          name = "vendor-encapsulated-options";
                          always-send = true;
                        }
                        {
                          name = "vendor-class-identifier";
                          data = "OpenMobility3G";
                          always-send = true;
                        }
                        {
                          name = "dect-omm-ip";
                          space = "vendor-encapsulated-options-space";
                          data = "10.40.42.55";
                          always-send = true;
                        }
                    ];
                }
            ];
            interfaces-config = {
              interfaces = [
                "ens18"
              ];
            };
            lease-database = {
              name = "/var/lib/kea/dhcp4.leases";
              persist = true;
              type = "memfile";
            };
            rebind-timer = 2000;
            renew-timer = 1000;
            subnet4 = [
              {
                pools = [
                  {
                    pool = "10.40.42.50 - 10.40.42.99";
                    client-class = "innovaphone";
                  }
                  {
                    pool = "10.40.42.100 - 10.40.42.120";
                    client-class = "dect";
                  }
                ];
                subnet = "10.40.42.0/24";
                option-data = [
                  {
                    name = "domain-name-servers";
                    data = "10.40.42.2";
                  }
                  {
                    name = "ntp-servers";
                    data = "10.40.42.2";
                  }
                  {
                    name = "routers";
                    data = "10.40.42.2";
                  }
                ];
              }
            ];
            valid-lifetime = 4000;
        };
    };

}