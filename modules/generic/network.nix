{
  networking.networkmanager.enable = true;  # Easiest to use and most distros use this by default.
  networking.hostName = "event-vm";
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.
  # networking.useDHCP = lib.mkDefault true;
  
  networking = {
    interfaces = {
      ens18.ipv4.addresses = [{
        address = "10.42.42.10";
        prefixLength = 23;
      }];
      ens19.ipv4.addresses = [{
        address = "10.40.42.5";
        prefixLength = 24;
      }];
    };
    usePredictableInterfaceNames = true;
    defaultGateway = "10.40.42.2";
    nameservers = ["10.42.42.1" "10.40.42.2"];
  };
  
  networking.firewall.enable = false;
}
