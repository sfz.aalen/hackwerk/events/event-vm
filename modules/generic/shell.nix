{ pkgs, ... }: {
  environment.shellAliases = {
    system-update = ''nixos-rebuild switch --flake gitlab:sfz.aalen%2Fhackwerk%2Fevents/event-vm/$(curl -s https://gitlab.com/api/v4/projects/51207114/repository/commits/ | ${pkgs.jq}/bin/jq -r -M ".[0].id")#eventvm'';
  };
}

