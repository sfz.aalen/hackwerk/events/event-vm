{ config, pkgs, ... }:

{
  imports =
    [
      ./efi.nix
      ./keymap.nix
      ./network.nix
      ./ssh.nix
      ./user.nix
      ./shell.nix
    ];

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    vim
    wget
    usbutils
    git
    htop
    tmux
  ];

  system.stateVersion = "23.05"; # Did you read the comment?
}

