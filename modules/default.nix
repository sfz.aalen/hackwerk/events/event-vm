{ config, pkgs, ... }:

{
  nix = {
    package = pkgs.nixFlakes;
    extraOptions = ''
      experimental-features = nix-command flakes
    '';
  };
  imports =
    [
      ./virtualisation.nix
      #./dhcpd.nix
      ./generic
      ./containers
      ./monitoring.nix
    ];
}

