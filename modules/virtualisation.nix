
{ config, pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    docker-client
    podman-compose
  ];

  # Setup DNS.
  services.resolved = { enable = true; };

  virtualisation = {
    docker.enable = false;
    podman = {
      enable = true;
      dockerSocket.enable = true;
      dockerCompat = true;
      defaultNetwork.settings.dns_enabled = true;
    };
  };

  virtualisation.oci-containers.backend = "podman";
}